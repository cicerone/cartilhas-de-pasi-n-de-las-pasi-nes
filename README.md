# Cartilhas traduzidas para pt_BR de jogos _Powered by The Apocalypse_

O objetivo deste trabalho inicialmente era traduzir as cartilhas de Pasión de las Pasiónes para o português do Brasil e disponibilizá-las em formato markdown, odt e pdf, mas com a divulgação da prévia de Monsterhearts segunda edição decidi expandi-lo para incluir as cartilhas em português dos jogos _PbtA_ que porventura sejam necessárias.  

Este trabalho está sendo disponibilizado sob licença Creative Commons CC-BY-SA 4.0. Para o texto completo da licença, [clique aqui](LICENSE), mas, resumidamente:

Você tem o direito de:  

+ Compartilhar — copiar e redistribuir o material em qualquer suporte ou formato;  
+ Adaptar — remixar, transformar, e criar a partir do material para qualquer fim, mesmo que comercial.  

Esta licença é aceitável para Trabalhos Culturais Livres. O licenciante não pode revogar estes direitos desde que você respeite os termos da licença.

De acordo com os termos seguintes:

+ Atribuição — Você deve dar o crédito apropriado, prover um link para a licença e indicar se mudanças foram feitas. Você deve fazê-lo em qualquer circunstância razoável, mas de nenhuma maneira que sugira que o licenciante apoia você ou o seu uso.  
+ CompartilhaIgual — Se você remixar, transformar, ou criar a partir do material, tem de distribuir as suas contribuições sob a mesma licença que o original.  
+ Sem restrições adicionais — Você não pode aplicar termos jurídicos ou medidas de caráter tecnológico que restrinjam legalmente outros de fazerem algo que a licença permita.  
