# Fantasma

_Você já teve um futuro antes. Crescer era um tumulto doloroso Às vezes, mas pelo menos você estava crescendo. Agora você só tem um passado - assuntos inacabados a tratar antes de deixar este mundo para trás._

_A vida é preciosa. Você entende isso, agora que perdeu a sua. Você só quer ajudar. Você só quer ser vista. Mas às vezes até o desejos mais simples parecem difíceis de compreender_.

_Fantasma fantasmagórica, você está morta._

## Jogando com A Fantasma

Sozinha, ferida, atenciosa, e sinistra. A Fantasma sofreu imensos traumas, e agora busca validação e intimidade. Ela tem o potencial de cuidar e curar outros, mas também tendem a ignorar limites físicos e pessoais.

As duas escolhas de atributos para a Fantasma te deixam ir em direção de ou gélida e distante (Frio 2 & Sombrio 1), ou assustadora e temperamental (Sombrio 2 & Volátil 1). Sua fantasma pode acabar suplicando por ajuda, afastando as pessoas que importam para ela, ou se exaurindo tentando cuidar dos demais.

No início do jogo, o atributo Quente baixo da Fantasma significa que não são boas em _Excitar Alguém_. Isso entra no dilema principal dela - sem poder social, como a Fantasma consegue a atenção e apoio emocional que precisa? Talvez seja infindavelmente concedente, esperando recíproca. _Espírito Prestativo_ e _Transferência_ ambos apontam nessa direção. Talvez seja maldosa e vingativa, assumindo de partida que não são dignasde afeto. _Trauma Não Resolvido_ e _Culpa Projetada_ oferecem uma dinâmica diferente, sugerindo uma Fantasma mesquinha e vingativa que ataca aqueles que a lembram do que perdeu. _Se Esgueirar_ e _Sem Limites_ adicionam um elemento voyeurístico, encorajando a Fantasma a ignorar os limites de outras pessoas. Várias possibilidades existem nas interações entre esses movimentos também.

## Identidade

**Nome**: Alastor, Avira, Catherine, Daniel, Kara, Lenora, Orville, Rufus, Spencer, Tien

**Aparência**: desesperada, assustada, abafada, deslocada, inquietante

**Olhos**: olhos vazios, olhos aflitos, olhos enfadonhos, olhos angustiantes, olhos penetrantes

**Origem**: deixada para morrer, assassinada a sangue frio, assassinada passionalmente, um acidente trágico, uma morte confusa

## Seu Passado

Alguém sabe que você está morta e como morreu. Essa pessoa recebe 2 Fios com você.

Você esteve dentro do quarto de alguém enquanto essa pessoa estava dormindo. Receba um Fio com ela.

## Atributos

Escolha:  

+ Quente -1, Frio 2, Volátil -1, Sombrio 1
+ Quente -1, Frio -1, Volátil 1, Sombrio 2

## Eu Mais Sombrio

Você se torna invisível, imperceptível. Ninguém pode te ver, te sentir, ouvir sua voz. Você ainda pode afetar objetos inanimados, mas essa é sua única via de comunicação. Você escapa seu Eu Mais Sombrio quando alguém reconhece sua presença, e demonstra o quanto te querem por perto.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Fantasma.
+ Escolha outro movimento de Fantasma.
+ Escolha um movimento de qualquer outra Pele.
+ Escolha um movimento de qualquer outra Pele.
+ Você reside em uma **Casa Assombrada**.

## Movimento Sexual

Quando você faz sexo com alguém, ambos podem fazer uma pergunta para o outro. Essa pergunta pode ser feita entre os personagens ou entre os jogadores. Elas devem ser respondidas honestamente e diretamente.

## Movimentos de Fantasma

Você recebe _Trauma Não Resolvido_, e escolha mais 2:

### Trauma Não Resolvido

Quando alguma coisa traz lembranças de sua morte, você trava e ganha o Estado traumatizado se não o tem ainda. Quando alguém ajudar a resolver esse Estado, ambos marcam experiência.

### Espírito Prestativo

Quando você ajuda alguém a resolver um Estado, receba um Fio com essa pessoa.

### Transferência

Quando você toma tempo para realmente ouvir sobre os problemas de alguém, a pessoa cura 1 Ferimento, e então transferem seus ferimentos remanescentes para você.

### Culpa Projetada

Enquanto você tiver o Estado traumatizado, você pode agir como se outros possuíssem o Estado culpados por minha morte.

### Se Esgueirar

Quando você silenciosamente testemunhar alguém em um de seus momentos mais privados, talvez dormindo ou aplicando maquiagem, receba um Fio com essa pessoa.

### Sem Limites

Você pode atravessar paredes e voar.
