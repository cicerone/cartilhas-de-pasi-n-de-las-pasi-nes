# A Rainha

_Você é uma das especiais. Uma beleza real. Você merece mais que o resto deste mundo deplorável. Você merece a vontade e adoração daqueles a seu redor_.

_E não é apenas por você ser melhor que eles. É porque você os faz melhores. Mais fortes, mas belos, completos. Eles não seriam nada sem você_.

## Jogando com a Rainha

Popular, perigosa, nojenta, e mandona. A Rainha tem um clique poderoso que serve como sua gangue. Lealdade e controle são cruciais se a Rainha quiser reter seu poder, mas cada pessoa no clique tem seu conjunto de necessidades e desejos a se lidar.

As duas escolhas de atributos para a Rainha te permitem ir em direção ou a desejável e mandona (Quente 2 & Frio 1) ou feroz e secretiva (Frio 2 & Sombrio 1). De qualquer forma, você não é muito boa em sujar suas mãos - com um atributo Volátil baixo, você depende dos outros para lutar suas batalhas e te manter segura.

Dependendo da origem e movimentos que você escolher, a Rainha pode ir desde uma adolescente humana mundana a horror cósmico bizarro. Mais do que qualquer outra Pele, você está no controle de quão sobrenatural torná-la. Você é uma capitã das líderes de torcida mandona, ou uma rainha alienígena aqui para repopular a terra com sua ninhada?

## Identidade

**Nome**: Burton, Brittany, Cordelia, Drake, Jacqueline, Kimball, Raymond, Reyes, Varun, Veronica

**Aparência**: estonteante, dominante, gélida, neurótica, faladeira

**Olhos**: olhos calculistas, olhos cativantes, olhos turvos, olhos vagos, olhos lindos

**Origem**: a mais popular, a mais perigosa, líder de culto, fonte da infecção, primogênita da consciência coletiva

## Seu Passado

Nomeie três personagens coadjuvantes que são membros de sua gangue. Receba um Fio com cada um deles.

Você acha alguém ameaçador. Dê a essa pessoa um Fio com você, e receba um Fio com ela.

## Atributos

Escolha:  

+ Quente 2, Frio 1, Volátil -1, Sombrio -1
+ Quente -1, Frio 2, Volátil -1, Sombrio 1

## Eu Mais Sombrio

Eles falharam com você. De novo. Toda essa bagunça é culpa deles, e por que você deveria sofrer as consequências da idiotice deles? Você precisa tornar cada um deles um exemplo -- um exemplo cruel e inabalável. Você escapa de seu Eu Mais Sombrio quando ceder parte de seu poder a alguém mais merecedor, ou quando destruir uma pessoa inocente para provar sua própria força.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Mortal.
+ Escolha outro movimento de Mortal.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Receba **O Clique** novamente e detalhe outra gangue.

## Movimento Sexual

Quando você fizer sexo com alguém, as pessoas que fizeram sexo com você ganham o Estado **um deles**. Enquanto o Estado se mantiver, eles contam como parte de sua gangue.

## Movimentos de Rainha

Você recebe _O Clique_, e escolha mais um:

### O Clique

Você é a líder do clique mais forte, mais legal, mais poderoso da área. Eles contam como uma gangue. Escolha uma das forças a seguir para sua gangue:

+ Ela tem armas (de fogo e coisas realmente perigosas),
+ Ela tem contatos (dinheiro e drogas de ponta),
+ Ela tem talento (uma banda ou equipe esportiva),
+ Ela é um culto (com votos sombrios e dispostos a morrer).

### O Escudo

Quando estiver cercada por sua gangue, subtraia 1 de qualquer rolagem contra você.

### Lealdade Comprada

Você pode dar a uma personagem coadjuvante um Fio com você para tentá-los a fazer sua vontade. A MC vai te dizer que tipo de suborno, ameaça ou coerção será necessária para fazer aquela personagem fazer o que você quer neste momento.

### E Seus Inimigos Mais Ainda

Quando alguém te trair, ganhe um Fio com essa pessoa.

### Muitos Corpos

Quando você prometer um membro de sua gangue para alguém, adicione 2 a sua rolagem para _Excitar Alguém_. Quando um membro de sua gangue fizer sexo com alguém, isso engatilha seu Movimento Sexual.

### Transmissão

Você tem uma conexão telepática com os membros de sua gangue. Você sempre pode ouvir suas emoções e medos. Se você tentar ouvir pensamentos específicos, _Contemple o Abismo_ e adicione 1 a sua rolagem.

