# A Vazia

_Eles começaram com o objetivo de fazer algo do nada. Não ficou claro se tiveram sucesso ou não. Veja você, no fim das contas tem muitos tons de cinza entre algo e nada_.

_Você está viva, mas não é real. Você não tem uma alma. Você não tem memórias de infância, já que não teve uma infância. Você não tem pais; tem criadores. E esses criadores esqueceram de te dar um lugar no mundo_.

## Jogando com a Vazia

Incerta, instável, impressionável, e perdida. A Vazia não tem um passado, e está penando para imaginar seu futuro. Ela está no meio de uma crise existencial, e sendo não-tão-real pode olhar apenas para os que estão ao seu redor em busca de respostas.

A Vazia anseia por um sentido de ser, e se agarrando a quaisquer rótulos que parecem poder ajudá-la a compor uma identidade, que é o motivo de tantos de seus movimentos girarem em volta de Estados.

Quando você usa _Impressões Estranhas_, você ganha qualquer dos Movimentos de Pele da ficha relevante. Você não é limitado apenas a aqueles que foram selecionados para aquela personagem. Quando você temporariamente ganhar um movimento dessa forma, isso não afeta o acesso da outra personagem a ele.

Se a Vazia fizer sexo com mais de uma pessoa de uma vez, todo mundo realiza a escrita e revelação simultaneamente. Se a Vazia compartilhou uma resposta com um ou mais personagens, esse é o grupo de personagens que marca experiência.

## Identidade

**Nome**: Adam, Baby, Bryce, Dorothy, Eva, Franklin, January, Max, Nix, Raymond, Summer

**Aparência**: imaculada, desgrenhada, assombrada, inexperiente, sincera

**Olhos**: olhos misteriosos, olhos desalmados, olhos vazios, olhos desesperados

**Origem**: nascida de um desejo, experimento falho, era um brinquedo, amnésica, máquina

## Seu Passado

Você tem aprendido a se comportar socialmente com alguém, e fazê-lo te ensinou muito sobre essa pessoa. Ganhe 2 Fios com essa pessoa.

Alguém enxerga através de seu passado inventado, e percebeu que é tudo mentira. Essa pessoa recebe 2 Fios com você.

## Atributos

Escolha:  

+ Quente 1, Frio -1, Volátil -1, Sombrio 2
+ Quente -1, Frio -1, Volátil 2, Sombrio 1

## Eu Mais Sombrio

Seu corpo é uma prisão. Você não pertence dentro dele. Você precisa colocá-lo no caminho do perigo, e fazê-lo sofrer, assim como ele te fez sofrer. Deve haver um jeito de se cortar para fora dele. Você precisa encontrar seus criadores, e fazê-los pagar pelo que fizeram com você. Para escapar de seu Eu Mais Sombrio, você deve achar uma forma de ver como alguém se sente mais aprisionado que você.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Vazia.
+ Escolha outro movimento de Vazia.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Você encontrou **Irmãos Vazios**.

## Movimento Sexual

Quando você fizer sexo com alguém, os jogadores dos personagens escrevem em segredo se o sexo foi desconcertante ou reconfortante para sua personagem. Se revelarem a mesma resposta, marquem experiência.

## Movimentos de Vazia

Escolha dois:

### Melhor que Nada

Quando você ganhar um Estado, marque experiência.

### Uma Tela em Branco

Quando realizar uma ação que personifica um de seus Estados, permitindo que aquele Estado altere seu senso de ser, risque-o e adicione 1 àquela rolagem.

### Tente com Mais Vontade na Próxima Vez

Quando você cometer uma gafe, se dê um Estado apropriado e receba 1 Adiante.

### De Mentira

Adicione 1 a quaisquer rolagens que fizer quando estiver mentindo.

### Metamorfose

Quando _Contemplar o Abismo_, em um 7 ou maior o abismo também te mostra o que você deve se tornar, e você pode permanentemente trocar os valores de 2 de seus atributos.

### Impressões Estranhas

Quando uma personagem principal te ferir ou te ajudar a se curar, você pode responder estudando a pessoa com olhos largos. Se o fizer, temporariamente ganhe um de seus Movimentos de Pele e o adicione a sua ficha de personagem. Ele desaparece assim que você o usar.
