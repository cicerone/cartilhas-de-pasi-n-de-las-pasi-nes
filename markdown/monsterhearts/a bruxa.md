# A Bruxa

_Em cada mecha de cabelo, cada olhar furtivo, cada recado secreto que troca de mãos durante a aula de história - é um convite. Um convite para ser importunado. Não que bruxaria seja sobre importunar os outros, exatamente, mas é difícil não notar quão perfeitamente maleável o mundo é, quando você sabe uma coisinha ou duas sobre magia_.

_Claro, uma bruxa boa como você conhece moderação. Uma bruxa boa deixa passar todos aqueles convites, e não pensa sobre quão doces vingança e controle podem ser. Uma bruxa boa está acima deste tipo de coisa. Na maior parte do tempo, pelo menos_.

## Jogando com a Bruxa

Inquietante, vingativa, secretiva, e oculta. A Bruxa espera o momento certo, silenciosamente julgando as pessoas até que uma oportunidade para retribuição mágica e travessuras se apresente.

As opções de atributos para a Bruxa se inclinam para ser ou calculista e peçonhenta (Frio 2 & Sombrio 1) ou sedutora e arrepiante (Quente 1 & Sombrio 2). De qualquer forma, a Bruxa depende de paciência para estar no auge de seu poder. A não ser que esteja disposta a entoar cânticos em línguas esquecidas, seus olhos nadando em redemoinhos carmesins, seu atributo Volátil baixo significa que não é muito boa em reagir a ameaças inesperadas.

A melhor forma de manter registro de Fios e Símbolos Simpatéticos é usar símbolos diferentes para cada um. Já que círculos já são sugeridos para Fios, pequenas estrelas ou triângulos funcionam bem para os Símbolos Simpatéticos. Você pode anotar o que os objetos físicos são num pedaço de papel à parte ou nas margens da ficha de personagem.

Se uma personagem de outra Pele escolher o movimento Feitiçaria sem também pegar Símbolos Simpatéticos, a única forma que ela pode conjurar um Feitiço é olhando nos olhos de seu alvo e entoando em línguas esquecidas - não é exatamente uma abordagem sutil.

## Identidade

**Nome**: Abrielle, Annalee, Cordelia, Darius, Evelyn, Gerard, Lucca, Merrill, Sabrina, Vanessa

**Aparência**: ágil, reservada, recatada, nervosa, meticulosa

**Olhos**: olhos calculistas, olhos sorridentes, olhos alegres, olhos maldosos, olhos profundos

**Origem**: ensinada pela avó, despertada, iniciada pagã, tumblr, leitora ávida

## Seu Passado

Você começa o jogo com dois Símbolos Simpatéticos. Decida de quem e o que são.

Um dos outros personagens te pegou mexendo nas coisas dos amigos dele, mas não disse nada. Essa pessoa tem um Fio com você.

## Atributos

Escolha:  

+ Quente -1, Frio 2, Volátil -1, Sombrio 1
+ Quente 1, Frio -1, Volátil -1, Sombrio 2

## Eu Mais Sombrio

O momento para sutileza e paciência passou. Você é poderosa demais para tolerar mais desses lixos. Você enfeitiça qualquer um que te menosprezar. Todos os seus feitiços têm efeitos colaterais inesperados, e são mais efetivos do que você normalmente gostaria. Para escapar de seu Eu Mais Sombrio, você deve oferecer paz a quem tenha machucado mais.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Bruxa.
+ Receba os Feitiços remanescentes.
+ Crie um novo Feitiço.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Você faz parte de uma **Irmandade**.

## Movimento Sexual

Após fazer sexo, você pode pegar um Símbolo Simpatético da pessoa. Ela sabe disso e está de boa.

## Movimentos de Bruxa

Você começa com _Símbolos Simpatéticos_ e _Feitiçaria_:

### Símbolos Simpatéticos

Você ganha poder de Símbolos Simpatéticos - item de significância pessoal tomados dos outros. Símbolos Simpatéticos contam como Fios.

### Feitiçaria

Você pode conjurar Feitiços. Escolha dois que você sabe. Para conjurá-los, ou gaste um Símbolo Simpatético durante um ritual secreto, ou olhe nos olhos do alvo e entoe cânticos em línguas esquecidas. Então role com Sombrio. Num 10 ou mais, o Feitiço funciona, e pode ser facilmente revertido. Num 7-9, ele funciona mas escolha um:

+ a conjuração te causa um Ferimento;
+ o Feitiço tem efeitos colaterais estranhos;
+ engatilhe seu Eu Mais Sombrio.

### Magia Transgressiva

Se seu ritual transgredir os padrões morais ou sexuais da comunidade, adicione 1 a sua rolagem de Feitiçaria.

### Santuário

Você tem um local secreto para praticar bruxaria. Adicione 1 a todas as rolagens que fizer neste espaço.

## Feitiços (escolha dois)

### Definhar

A pessoa enfeitiçada perde todo seu cabelo, ou seus dentes começam a cair, ou sua menstruação chega inesperada e pesada, ou sua pele fica toda amarela e marcada. O que quer que sejam os específicos, o negócio é feio.

### Amarração

A pessoa não pode fisicamente ferir outros.

### Anel de Mentiras

Sempre que a pessoa tentar mentir, ela ouve um zumbido agudo. Grandes mentiras vão fazer seus joelhos falharem e desorientá-la. Mentiras graves podem causar ferimentos ou até dano cerebral.

### Observação

Você entra em um sono profundo, e começa a ver o mundo através dos olhos da pessoa enfeitiçada. Você pode sentir suas reações e impressões sobre o que estão vendo.

### Ilusões

Escolha um: cobras e insetos, rostos demoníacos, profecias falsas, subtexto inexistente. A pessoa enfeitiçada vê aquilo por todo lugar. Você não tem controle sobre as imagens ou manifestações exatas.

