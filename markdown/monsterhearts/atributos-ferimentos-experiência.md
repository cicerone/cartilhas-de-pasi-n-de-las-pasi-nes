# Atributos

| Atributo | Valor | Movimentos gerais |
| :---: | :---: | :---: |
| Quente | | _Excitar alguém_ |
| Frio | | _Calar alguém_, _Manter a Calma_ |
| Volátil | | _Atacar fisicamente_, _Fugir_ |
| Sombrio | | _Contemplar o Abismo_ |

# Ferimentos ☐ ☐ ☐ ☐

# Adiante 1

---

---

---

---

---

# Estados

---

---

---

---

---

# Fios

---

---

---

---

---

---

---

---

# Experiência ☐ ☐ ☐ ☐ ☐

