# O Licantropo

_Todo mundo ao seu redor parece tão disposto a seguir o papel designado a eles, a quietamente colorindo dentro das linhas. Eles foram domados, domesticados. Você é de uma linhagem diferente: você quebrou a cerca construída para te conter. Você uivou para a lua, e ouviu ela uivar de volta_.

_Agora, a transformação está completa. Isso é o que você sempre deveria ter sido. Selvagem. Inabalável. Vivo_.

## Jogando com o Licantropo

Agressivo, dominante, primal, e amoroso. O Licantropo é armado para a violência, e sabe que a dominância física é a raiz do poder social. Ele é territorial e perigoso, mas atrai pessoas com seu deslumbramento bruto e sensual. O Licantropo é finalizado com um lado místico, animal: ele é mais forte quando banhado pelo luar e guiado por instintos primais.

As opções de atributos destacam a natureza sexy e perigosa do Licantropo. Sua escolha é entre pender mais para um destruidor de corações com um lado maldoso (Quente 2 & Volátil 1), ou um porra louca imprevisível de quem é um perigo se aproximar (Quente 1 & Volátil 2).

A pergunta sobre poder se transformar para a forma de lobo quando você não é seu Eu Mais Sombrio é deixado para grupos individuais decidirem. Você tem os mesmos atributos e movimentos independente de sua forma.

## Identidade

**Nome**: Cassidy, Candika, Flinch, Levi, Margot, Lorrie, Luna, Peter, Tucker, Zachary

**Aparência**: primal, desleixada, rija, robusta, exuberante

**Olhos**: olhos astutos, olhos predatórios, olhos sagazes, olhos selvagens, olhos lupinos

**Origem**: nascido um lobo, mordido, criado por lobos, poder ancestral, desperto, favorecido pela lua

## Seu Passado

Você não tem sutileza. Dê um Fio para todo mundo.

Você passou semanas observando alguém de longe. Seu cheiro e maneirismos são inconfundíveis para você agora. Receba dois Fios com essa pessoa.

## Atributos

Escolha:  

+ Quente 1, Frio -1, Volátil 2, Sombrio -1
+ Quente 2, Frio -1, Volátil 1, Sombrio -1

## Eu Mais Sombrio

Você se transforma numa criatura lupina aterrorizante. Você anseia por poder e dominância, e esses são obtidos com derramamento de sangue. Se qualquer pessoa tenta ficar no seu caminho, ela deve ser derrubada e sangrar. Você escapa de seu Eu Mais Sombrio quando ferir alguém com quem você realmente se importe ou o sol se erguer, o que vier primeiro.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Licantropo.
+ Escolha outro movimento de Licantropo.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Você faz parte de uma **Alcatéia de Lobos**.

## Movimento Sexual

Quando você fizer sexo com alguém, você estabelece uma conexão espiritual profunda com essa pessoa. Até um de vocês quebrar essa conexão espiritual (ao fazer sexo com outra pessoa) adicione 1 a rolagens para defendê-la. Você sabe quando a conexão é quebrada.

## Movimentos de Licantropo

Escolha dois:

### Dominância

Quando você ferir alguém, receba um Fio com essa pessoa.

### Cheiro de Sangue

Adicione 1 a todas as rolagens contra aqueles que já foram feridos nesta cena.

### Uivar Para a Lua

Quando banhado em luar, você pode agir como se tivesse Sombrio 3.

### Armadura Espiritual

Quando banhado pelo luar, quaisquer ferimentos que você sofrer são reduzidos em 1, e você adiciona 2 a rolagens para _Manter a Calma_.

### Sentidos Aguçados

Quando depender de seus sentidos aguçados para compreender uma situação carregada, role com Sombrio. Num 10 ou mais, faça três perguntas à MC da lista abaixo e receba 1 Adiante. Num 7-9, faça um pergunta da lista abaixo e receba 1 Adiante:

+ Qual é minha melhor rota de fuga ou caminho de entrada?
+ Qual inimigo é o mais vulnerável a mim?
+ Qual é a fraqueza secreta dessa pessoa?
+ Quem representa a maior ameaça a mim?
+ Quem está no controle aqui?

### Instável

Quando você se torna seu Eu Mais Sombrio, marque experiência.

