# A Carniçal

_A morte te mudou. Te tirou sua felicidade contemplativa, dessensibilizou seus sentidos, e te deixou impossivelmente faminta. Essa fome está sempre com você, como um zumbido em seus ouvidos que se avoluma e cresce até que não consiga ouvir mais nada. Ignorada, ele pode acabar te dominando - mas ceder a ela pode ser tão ruim quanto_.

_Há uma certa beleza no que você se tornou. Seu corpo esquálido, sua forma sobrenatural - ela chama as pessoas. Seu desinteresse total é cativante. Mas por trás dessa apresentação descontente - a fome, a fome_.

## Jogando com a Carniçal

Obsessiva, perigosa, mórbida e quieta. A Carniçal está constantemente lidando com uma Fome voraz, e a distância emocional trazida pela morte torna fácil fazer coisas ruins ao perseguir alimento. Ela pode ser um zumbi devorador de carne, ou algo um pouco mais sutil e estranho.

As duas escolhas de atributos para a Carniçal pintam a personagem como ou cruel e errática (Volátil 2 & Frio 1) ou descontente e portentosa (Frio 2 & Sombrio 1). Já que _A Fome_ te força _Manter a Calma_ para deixar passar uma oportunidade de se saciar, seu atributo Frio tem um papel pivotal na manutenção de seu autocontrole.

_Golem Vigilante_ e _Esprit de Cadáver_ apresentam a oportunidade de conduzir a Carniçal em duas direções diferentes. Você está cuidando de outros devido a um profundo mas não expresso sentimento de carinho, ou anda por aí os servindo pois a morte te tirou sua sensação de independência?

_Descanso Curto para os Ímpios é uma receita para pandemônio_. Também é um convite para a MC te colocar numa situação nova e dramática. Muito pode acontecer em algumas horas.

Seu Movimento Sexual te chama a criar uma nova Fome. Pode ser qualquer coisa que você quiser, e é adicionada a sua personagem ao lado das Fomes existentes. Se você fizer sexo frequentemente, notará seu apetite se tornando mais abrangente e bizarro a todo momento.

## Identidade

**Nome**: Akuji, Cage, Cole, Georgia, Horace, Iggy, Mara, Morrigan, Silas, Sharona, Victor, Zed

**Aparência**: esquálida, rígida, desfigurada, distante, acabada

**Olhos**: olhos vazios, olhos quietos, olhos calculistas, olhos sérios, olhos famintos

**Origem**: ressuscitada, construída, perturbada, rejeitada, enviada

## Seu Passado

Alguém te lembrou do que é o amor, quando você pensava que a morte lhe havia roubado isso para sempre. Dê a essa pessoa um Fio.

Alguém te viu morrer? Se sim, cada um ganha 2 Fios sobre o outro.

## Atributos

Escolha:  

+ Quente -1, Frio 1, Volátil 2, Sombrio -1
+ Quente -1, Frio 2, Volátil -1, Sombrio 1

## Eu Mais Sombrio

Sua fome rotineira se aguça. Você não consegue se focar em nada que não seja se saciar. E em adição a seus desejos peculiares, você reconhece algo a mais. Aquela fome primordial que conecta todas as outras. Carne, sangue. Você escapa de seu Eu Mais Sombrio assim que houver exagerado no seu consumo, ou tenha ficado preso por tempo o suficiente para se recompor.

## Avanço

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Carniçal.
+ Escolha outro movimento de Carniçal.
+ Escolha um movimento de qualquer outra Pele.
+ Escolha um movimento de qualquer outra Pele.
+ Você se torna parte de uma **Trupe Inconsequente**.

## Movimento Sexual

Quando fizer sexo com alguém, crie uma nova Fome.

## Movimentos de Carniçal

Você recebe _A Fome_, e escolha mais dois:

### A Fome

Você tem Fome de (circule 1): medo, poder, pilhagem, adrenalina.  

Quando você negligentemente perseguir uma Fome, adicione 1 a suas rolagens. Quando ignorar uma oportunidade promissora de se saciar, role para _Manter a Calma_.

### O Que a Mão Direita Quer

Seu corpo contém muitas histórias, e deseja muitas coisas. Crie outra Fome.

### Saciedade

Quando você saciar uma Fome, escolha um:
+ cure 1 Ferimento;
+ marque experiência;
+ receba 1 Adiante.

### Descanso Curto para os Ímpios

Quando você morrer, espere. Algumas horas depois, você acorda completamente curada.

### Golem Vigilante

Quando você defende alguém sem a pessoa descobrir sobre isso, marque experiência.

### O Fim

Você lembra de cada detalhe de sua morte. Quando você falar a alguém sobre isso, lhes dê o Estado mórbido e role para _Excitar Alguém_ com Frio.

### Esprit de Cadáver

Quando você _Contemplar o Abismo_, o abismo vai compartilhar com você sua Fome. Trate aquela Fome como uma das suas até saciá-la, e marque experiência quando o fizer.
