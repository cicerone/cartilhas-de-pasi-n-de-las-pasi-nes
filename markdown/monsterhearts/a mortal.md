# A Mortal

_Nenhum deles entenderia. O que você tem aqui, nesse lugar secreto e sombrio, é lindo. Eles te alertariam que esse tipo de beleza é perigosa, como um fogo devastador. Bem algumas coisas fazem se queimar valer a pena_.


_O amor eclipsou toda esperança, e a escuridão te deixou se sentindo linda_.

## Jogando com a Mortal

Vulnerável, magnética, e linda. Para qualquer outra Pele, dar um Fio representaria uma perda de controle. Para você, é mais simbiótico - você ganha poder ao cedê-lo. A Mortal explora co-dependência, desequilíbrios de poder, e expectativa esperançosa.

As duas escolhas de atributo para a Mortal tem Quente 2, pois a Mortal é desejável e especial. Eles diferem dependendo se a Mortal é mais impulsiva e apavorada (Volátil 1) ou inquietante e solitária (Sombrio 1).

_Amor Verdadeiro_ é sobre quem está nesse momento no centro de seu universo. Você não necessariamente precisa estar em um relacionamento com alguém para declarar essa pessoa sua Amante.

Seu Movimento Sexual pode parecer uma grande desvantagem, mas lembre-se que a Mortal pode ganhar bastante alavancagem de vitimismo. Ter amantes subitamente se tornando estranhos, assustadores ou hostis depois de um momento de intimidade te dá a oportunidade perfeita para tirar vantagem de movimentos como _Simpatia É Minha Arma_, _Desculpas São Minha Armadura_, e _Descendo Pela Toca do Coelho_.

## Identidade

**Nome**: Anne, Carla, Deirdre, James, Jonathan, Laeli, Patrick, Robin, Shen, Timothy, Wendy

**Aparência**: quieta, desesperada, esquisita, linda, deslocada

**Olhos**: olhos inocentes, olhos tristes, olhos descontrolados, olhos nervosos, olhos humanos

**Origem**: nova garota na cidade, garota familiar, a barista, namorada de alguém, namorado de alguém, ninguém

## Seu Passado

Declare seu passo por último.

Escolha uma pessoa para ser sua Amante. Dê a ela 3 Fios com você. Receba 1 Fio com ela.

## Atributos

Escolha:  

+ Quente 2, Frio -1, Volátil -1, Sombrio 1
+ Quente 2, Frio -1, Volátil 1, Sombrio -1

## Eu Mais Sombrio

Ninguém te entende. Nem sequer tentam. Você faz tanto pelas pessoas que você ama, e elas pisam em você. Basta! Traia essas pessoas. Mostre a elas o que é ser desprezada. Revele a monstruosidade delas e a sua. Apenas ver a dor que você está causando ao seu Amante vai te deixar escapar de seu Eu Mais Sombrio.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Mortal.
+ Escolha outro movimento de Mortal.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.

## Movimento Sexual

Quando você fizer sexo com alguém, isso desperta algo sinistro de dentro deles. A próxima vez que você tirar os olhos deles, eles se tornam seu Eu Mais Sombrio.

## Movimentos de Mortal

Você ganha _Amor Verdadeiro_, e escolha mais dois:

### Amor Verdadeiro

Você sempre tem exatamente um Amante. O primeiro é escolhido durante Seu Passado. Se você em qualquer momento se apaixonar por outra pessoa, dê a ela um Fio com você e ela se torna sua nova Amante. Você sempre tem 1 Adiante para conquistar a preferência ou o coração de sua Amante.

### Mexeu Comigo, Mexeu com Ele

Quando usar o nome de seu Amante como uma ameaça, adicione 2 a sua rolagem de _Calar Alguém_ ou _Manter a Calma_. Sua Amante ganha um Fio com você.

### Entrincheirada

Se você e outra personagem tiverem um total combinado de 5 Fios uma com a outra, adicione 1 a suas rolagens contra ela.

### Simpatia É Minha Arma

Toda vez que você perdoar alguém por te magoar, justificando com a natureza base da pessoa, ganhe um Fio com ela.

### Desculpas São Minha Armadura

Quando você ignorar algum problema gritante com seu Amante ou como te trata, marque experiência.

### Espiral Descendente

Quando você _Contemplar o Abismo_, você pode se causar 1 Ferimento. Se o fizer, adicione 2 à rolagem.

### Descendo Pela Toca do Coelho

Quando você meter seu nariz em assuntos que sua espécie não deveria se meter, alguém envolvido na situação ganha um Fio com você, e você marca experiência.
