# O Vampiro

_Você é beleza eterna. Você é a escuridão que todos querem provar, mas ninguém ousa compreender. Está lá em seus olhos, suas palavras cuidadosamente escolhidas, e cada gesto seu: você não tem mais uma alma_.

_Alguns vampiros se deleitam nesse fato, seu além-morte uma tapeçaria de hedonismo e exsanguinação. Outros odeiam o mal em sua pele, jurando solenemente uma existência casta e solitária. De qualquer forma, alguém sofre. A escolha é sua_.

## Jogando com o Vampiro

Gélido, manipulativo, hipnótico, e cruel. O Vampiro prospera em entrincheiramento emocional e controle. O Vampiro sabe como enfraquecer a vontade dos outros, e geralmente possui uma atitude desconcertante quando se trata de consentimento.

As opções de atributos demonstram a natureza Quente e Fria do Vampiro, ardentemente romântico em um minuto e simplesmente maldoso no próximo. Sua escolha é para que lado a balança tende a pender: sexy ou desdenhoso.

O Vampiro tem alguns movimentos que são simplesmente assustadores, não por causa de nada sobrenatural, mas por serem calculadamente e intimamente violentos. Jogar com o Vampiro é lidar com uma pessoa que propositalmente machuca os outros. Você trabalha em direção a redenção? Você cede à tentação sombria? Lembre-se que você é uma personagem principal nessa história, e isso significa tem um arco de personagem além de somente ferir os outros. Ou, se não, prepare-se para ver seu papel mudar de protagonista para vilão, à medida que os demais personagens afiam suas estacas.

## Identidade

**Nome**: Amanda, Cassius, Clayton, Helene, Isaiah, Jessamine, Jong, Lucian, Marcell, Morana, Serina

**Aparência**: intensa, indiferente, predatória, ardente, antiga

**Olhos**: olhos mortos, olhos sensuais, olhos sofridos, olhos famintos, olhos sedentos

**Origem**: recém renascido, tomado neste século, eras de idade, lorde, sangue amaldiçoado

## Seu Passado

Você é lindo. Ganhe um Fio com todo mundo.

Alguém salvou sua morte-vida uma vez. Eles ganham 2 Fios com você.

## Atributos

Escolha:  

+ Quente 2, Frio 1, Volátil -1, Sombrio -1
+ Quente 1, Frio 2, Volátil -1, Sombrio -1

## Eu Mais Sombrio

Todos são seus peões, seus brinquedos. Você os machuca e os deixa vulnerável, por esperto - como um gato faz com um rato. Talvez você até os drene até ficarem secos, embora você certamente vai aproveitar antes. Você escapa de seu Eu Mais Sombrio quando é colocado em seu devido lugar, por alguém mais poderoso que você.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Vampiro.
+ Escolha outro movimento de Vampiro.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Você faz parte de uma **Camarilha de Vampiros**.

## Movimento Sexual

Quando você nega alguém sexualmente, ganhe um Fio com essa pessoa. Quando você fizer sexo com alguém, perca todos os Fios com aquela pessoa.

## Movimentos de Vampiro

Escolha dois:

### Convidado

Você não pode entrar em uma casa sem ser convidado. Quando que alguém te convidar para entrar, receba um Fio com essa pessoa.

### Hipnótico

Você consegue hipnotizar pessoas que não tem Fios com você. Role com Quente.

+ Num 10 ou mais, elas fazem exatamente o que você quer e não tem nem ideia de que algo está errado.
+ Num 7-9, a hipnose funciona, mas escolha um:
+ elas percebem exatamente o que você fez,
+ elas cagam seus comandos,
+ sua sanidade é comprometida.

### Frio como Gelo

Quando você _Calar Alguém_ e rolar um 7 ou mais alto, você pode escolher uma opção extra da lista.

### A Alimentação

Você se alimenta de sangue quente, direto da fonte. Se essa for a primeira vez que a pessoa serve de alimento, ambos marcam experiência. Quando se alimentar, escolha dois:

+ Você cura 1 Ferimento,
+ Você recebe 1 Adiante,
+ A pessoa definitivamente não morre.

### Marcado para a Caça

Se alimentar em alguém estabelece um elo preternatural. Daquele ponto em diante, sempre que você _Contemplar o Abismo_ com relação à localização ou bem-estar dessa pessoa, role como se tivesse Sombrio 3.

### Inescapável

Você pode gastar um Fio com alguém para demandar que essa pessoa permaneça em sua presença. Se mesmo assim ela for embora, receba 2 Fios com essa pessoa.

