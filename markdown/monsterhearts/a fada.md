# A Fada

_Às bordas deste mundo, logo depois do véu, existem cores que poucos mortais sequer podem sonhar. Beleza o suficiente para despedaçar qualquer coração. As fadas vivem e respiram às bordas desse mundo. Eles mantém um pouquinho daquela magia atrá de suas orelhas, só por precaução._

_E as Fadas estão dispostas a compartilhar. Elas não são nada senão generosas, pedindo apenas uma coisa em retorno. Uma promessa. A mantenha, e a verdadeira beleza do mundo vai ser revelada. Quebre-a, e sinta a fúria da vingança feérica._

## Jogando com A Fada

Fascinante, de outro mundo, inconstante, e vingativa. A Fada tenta pessoas a fazer promessas, e exerce vingança feérica quando essas promessas são quebradas. Elas também tem o poder de comungar com forças etéreas, de além do véu.

As duas escolhas de atributos para a Fada permite que se escolha ser ou bela e misteriosa (Quente 2 & Sombrio 1) ou audaciosa e alienígena (Volátil 2 & Quente 1). Com um Frio consistentemente baixo, ela não é muito inclinada a ser calma, cínica, ou irônica. De sua
sensualidade para a frente a seu invariável senso de justiça, sinceridade é um grande tema para a Fada.

Quando joga interpreta a Fada, promessas importam. Use a sensualidade e sagacidade da Fada para provocar promessas de personagens. Você pode adicionar incentivos mecânicos para que outros façam promessas a você gastando Fios para _tentá-los a fazer o que você quer_, ou através do Movimento _Seduzir_. Mantenha registro das promessas que os outros fazem para você, nas margens de sua ficha de personagem ou em papel de rascunho.

_Além do Véu, Guia_, a opção de se juntar a um **Júri das Fadas**, e falar de justiça feérica todos convidam a colaborativamente imaginar o mundo das fadas. Para fazê-lo, faça perguntas para a MC, antecipe perguntas que podem ser feitas a você como resposta, e se prepare para surpresas.

## Identidade

**Nome**: Anders, Aurora, Crow, Gail, Harmony, Iris, Lilith, Ping, Selene, Sienna, Walthus

**Aparência**: delicada, feminina, magra, misteriosa, desgrenhada

**Olhos**: olhos rápidos, olhos líricos, olhos hipnotizantes, olhos sorridentes, olhos penetrantes

**Origem**: nascida como fada, linhagem de fada, trocada no nascimento, roubou o dom, tocada pelo dom

## Seu Passado

Você tem emoções à flor da pele. Dê a todos um Fio.

Você capturou o apreço de alguém. Ganhe 2 Fios com essa pessoa.

## Atributos

Escolha:  

+ Quente 2, Frio -1, Volátil -1, Sombrio 1
+ Quente 1, Frio -1, Volátil 2, Sombrio -1

## Eu Mais Sombrio

Tudo o que você diz parece uma promessa. Tudo o que você ouve parece uma promessa. Se uma promessa é quebrada, justiça deve ser feita em truques ou sangue. Você não está sujeita às regras humanas de misericórdia. Para escapar de seu Eu Mais Sombrio, você deve de alguma forma re-equilibrar as balanças da justiça.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Fada.
+ Escolha outro movimento de Fada.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Você faz parte de um **Júri das Fadas**.

## Movimento Sexual

Quando se deita nua com alguém, você pode lhes pedir uma promessa. Se a pessoa negar, receba 2 Fios com ela.

## Movimentos de Fada

Você recebe _Contrato Feérico_, e escolha mais um:


### Contrato Feérico

Se alguém quebra uma promessa ou contrato firmado com você, receba um Fio com eles. Quando gastar um Fio para acertar as contas e conseguir justiça por uma promessa quebrada, adicione essas opções a _Puxando Fios_:

+ Eles fazem merda em uma coisa simples em um momento crucial, sofrendo 1 Ferimento se apropriado,
+ adicione 2 a sua rolagem num ato de vingança.

### Sem Vergonha

Você pode dar um Fio a alguém com você para adicionar 3 a sua tentativa de _Excitar Alguém_.

### A Caçada Selvagem

Quando você faz uso de sua forma mais feral, ecoando os movimentos ágeis de um gato ou a voracidade de um lobo, adicione 1 a sua rolagem para _Excitar Alguém_.

### Seduzir

Quando alguém fizer uma promessa a você, essa pessoa marca experiência. Quando alguém quebra uma promessa feita a você, você marca experiência.

### Guia

Se você gastar um Fio com alguém disposto, você pode carregá-los através do véu, para dentro do reino das fadas. A magia dura por uma cena ou duas, antes de ambos serem retornados ao mundo normal.

### Além do Véu

Para pedir uma audiência com o Rei das Fadas, _Contemple o Abismo_. Em um 10 ou mais, além dos demais resultados, o Rei das Fadas revela a você um Fio escondido com alguém. receba-o. Em um 7 a 9, além dos demais resultados, o Rei das Fadas demanda um favor de você
