# O Infernal

_No início, parecia inocente. Ele te dava coisas, te fazia se sentir bem sobre você mesmo. Você vinha para ele com seus problemas, e ele consertava. Quando você perguntou como poderia retornar o favor, ele te disse para ser paciência - todas as dívidas seriam pagas em seu devido tempo. Essa foi a primeira vez que você o ouviu mencionar dívidas_.

_Você tem Satã do seu lado, ou um demônio em seu cérebro. Ou talvez as estrelas simplesmente se alinhem para você. De qualquer forma, você deve para algo muito maior e mais assustador do que você jamais será_.

## Jogando com o Infernal

Tentado, impulsivo, e em apuros. O Infernal tem um patrono demoníaco - alguém que lhe dá as coisas que ele quer, a um preço não-especificado. O Infernal brinca com os temas de tentação, vício, e dependência.

O Infernal é extremamente poderoso enquanto se endivida com seu Poder Sombrio, embora isso os leve a uma queda inevitável. A queda não é uma punição a ser evitada, mas um clímax no arco da personagem. Quando você joga com o Infernal, não pare logo antes daquele quinto Fio de dívida ou tente jogar de forma segura. O Infernal é mais interessante quando está variando caoticamente entre poderoso e impotente.

Com _Recrutador Sombrio_, os específicos do que significa trazer alguém para o Poder Sombrio fica por conta da interpretação e do contexto. Pode envolver um ritual de sacrifício, ou uma apresentação em uma cafeteria.

A barganha _Contrapartidas_ brinca com o tropo “cuidado com o que deseja” das histórias de bruxas e gênios. Escolher esta Barganha comunica à MC que você quer levar um soco no estômago por ironia trágica de vez em quando.

## Identidade

**Nome**: Baron, Cain, Chloe, Damien, Logan, Mark, Mika, Omar, Ophelia, Poe, Yoanna

**Aparência**: quieta, agitada, peçonhenta, mimada, assombrada

**Olhos**: olhos vazios, olhos calculistas, olhos ardentes, olhos cintilantes, olhos penetrantes

**Origem**: alma vendida, emissário, última chance, legião, lacaio, escolhido

## Seu Passado

Você tem dívidas. Dê 3 Fios, divididos da forma que quiser entre o Poder Sombrio e os outros personagens.

Alguém pensa que pode te salvar. Receba um Fio com essa pessoa.

## Atributos

Escolha:  

+ Quente -1, Frio -1, Volátil 2, Sombrio 1
+ Quente 1, Frio -1, Volátil -1, Sombrio 2

## Eu Mais Sombrio

Você se encontra tremendo, carente, e sozinho. O Poder Sombrio vai fazer algumas demandas pouco específicas e intimidadoras. Você escapa de seu Eu Mais Sombrio quando o Poder Sombrio não tiver mais Fios, ou você fizer uma barganha com uma entidade ainda mais perigosa.

## Avanços

+ Adicione +1 a um de seus atributos.
+ Escolha outro movimento de Infernal.
+ Receba as Barganhas remanescentes.
+ Escolha um movimento de qualquer Pele.
+ Escolha um movimento de qualquer Pele.
+ Você fornece para **Amigos Carentes**.

## Movimento Sexual

Quando você fizer sexo, o Poder Sombrio perde um Fio com você e ganha um Fio com quem quer que você tenha feito sexo.

## Movimentos de Infernal

Você recebe _Débito de Alma_, e escolha mais um:

### Débito de Alma

Você deve para um Poder Sombrio. O nomeie, e escolha duas Barganhas que ele tenha feito com você. O Poder Sombrio pode ganhar Fios. Se ele em algum momento possuir 5 Fios com você, engatilhe seu Eu Mais Sombrio.

### Recrutador Sombrio

Quando trouxer uma alma inocente para seu Poder Sombrio, marque experiência.

### Sob Pressão

Se alguém possuir 3 Fios com você, adicione 1 a suas rolagens quando seguir seus desejos.

### Não Posso Me Salvar

Quando alguém te salvar de forças grandes demais para você conseguir lidar, essa pessoa marca experiência, e você ganha um Fio com ela.

## Barganhas

Escolha duas Barganhas que o poder Sombrio tenha feito com você:

### O Poder Flui Através de Você

Você pode dar um Fio ao Poder Sombrio para adicionar 2 a sua próxima rolagem.

### Dessensibilizando

Você pode dar um Fio ao Poder Sombrio para remover um Estado ou até 2 Ferimentos.

### Poder de Outro Lugar

Você pode dar um Fio ao Poder Sombrio para usar um movimento que você não tem, só dessa vez. Esse movimento pode vir de qualquer Pele.

### Vozes Misteriosas

Você pode dar um Fio ao Poder Sombrio para descobrir um segredo sobre alguém com quem você está falando. A jogadora daquela personagem vai revelar um de seus medos secretos, desejos secretos, ou forças secretas (ela escolhe qual.)

### Contrapartidas

Você pode pedir algo para o Poder Sombrio que você queria, realmente queira. A MC vai colocar um preço na coisa que você quer, e sugerir uma reviravolta indesejada em sua natureza. Se você pagar o preço, você consegue o que você quer.
