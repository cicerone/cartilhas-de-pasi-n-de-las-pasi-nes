# A Bela (_La Belleza_)

![A Bela](images/image-000-150.png)

## Escolha um nome:
Constanza, Marcela, Soraya, Regina, Verónica, Violeta

## Sua questão:
Você é o centro das atenções?

## Aparência:
Quais são as duas coisas que as pessoas notam em você?

## Escolha até três adereços:
| Adereços | Adereços |
| :--- | :--- |
| [&emsp;] Um vestido que combine com a sala | [&emsp;] Sapatos de grife |
| [&emsp;] Um apartamento finamente decorado | [&emsp;] Um motorista que cuida das coisas |
| [&emsp;] Um par de óculos escuros imensos | [&emsp;] Um estilista sempre à disposição |
| [&emsp;] Um leque estilizado | [&emsp;] A melhor mesa em um restaurante |
| [&emsp;] Um cachorro dentro da bolsa | [&emsp;] Acesso a todos os clubes exclusivos |

## Relacionamentos:
______ já te viu chorar. Qual era o motivo de suas lágrimas?  
______ tem resistido aos seus charmes. Até agora.  

## Neste capítulo:
Você tem algo valioso de que precisa se desfazer. O que seria?

## Reação da audiência:
Marque experiência quando um membro da família assobiar em apreciação à sua beleza.

## Movimentos (escolha dois)

### [&emsp;] Grande entrada
Quando você entrar em cena, empregue uma Influência para forçar alguém a olhar óbvia e fixamente para você e agir desesperadamente antes de agir contra você. Se esse alguém rolar 7-9, ao invés do MC determinar um custo/complicação/qualquer coisa, você toma alguma coisa desse alguém ou recupera a Influência gasta, ao seu critério. Se esse alguém falhar, você obtém ambos os benefícios ao invés do MC realizar um movimento.

### [&emsp;] Rainha de Gelo
Quando você rejeitar alguém imediatamente após **expressar seu amor passionalmente**, desmarque 1 estresse ou obtenha Influência sobre esse alguém.

### [&emsp;] Anime-se
Quando você estiver incitando alguém enquanto parecer indiferente, adicione a questão "Você está fisicamente o tocando?" ao movimento **expressar seu amor passionalmente**.

### [&emsp;] Escorregadia
Quando alguém tentar expressar amor por você, acusá-lo de mentir, ou tentar perceber alguma coisa estranha, você pode interferir. Role com as questões:  

+ Esse alguém está sendo cruel?
+ Você está em público?  

Em um acerto, esse alguém recebe -2 em sua rolagem. Em um 10+, você também obtém Influência sobre esse alguém ou desmarca 1 estresse. Em caso de falha, esse alguém recebe um 10+ em sua rolagem, não interessa o que ele estava rolando, e você concede a esse alguém Influência sobre você.

## Estresses [&emsp;] [ ★ ] [&emsp;] [ ◆ ] [ ★ ] [ ◆ ] [&emsp;] [ M ]

Para cada ★ marque um:

[&emsp;] Distraído (-2 para **Lidar com Sentimentos** & **Perceber Algo**  
[&emsp;] Desgastado (-2 para **Expressar Amor** & **Exigir o que Você Merece**  

Para cada ◆ marque um:

[&emsp;] Amargurado (+1 para **Acusar Alguém de Estar Mentindo**)  
[&emsp;] Irado (+1 para **Agredir com Voz ou Violência**)  

Quando você atingir **M**, desmarque todo o seu estresse e escolha um:

+ Gaste toda a sua Influência sobre os outros;  
+ Conceda uma Influência para todos os outros;  
+ Seja retirado de jogo.  

## Influência
______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

## Avanço
[&emsp;] [&emsp;] [&emsp;] [&emsp;] [ ▶ ] *Avanço*

[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha mais três adereços  
