# O Chefe (_El Jefe_)

![O Chefe](images/image-006-150.png)

# Escolha um nome:
_El Generalíssimo_, Eladio, Ernesto, Federico, Roberto

# Sua questão:
Você está assumindo o controle da situação?

# Aparência:
Quais são as duas coisas que as pessoas notam em você?

## Escolha até três adereços:
| Adereços | Adereços |
| :--- | :--- |
| [&emsp;] Sua mansão ou negócios | [&emsp;] Uma pistola descomunal |
| [&emsp;] Um guarda atarracado | [&emsp;] Uma montanha de dinheiro |
| [&emsp;] Uma limusine preta grande | [&emsp;] Uma faca grande e assustadora |
| [&emsp;] Um caminhão cheio de drogas | [&emsp;] Um tapa-olho |
| [&emsp;] Um uniforme militar datado | [&emsp;] Um helicóptero |

## Relacionamentos:
______ costumava trabalhar para você. Como a parceria acabou?  
______ é dona de uma beleza que você deseja possuir. Como essa pessoa fugiu de você?

## Neste capítulo:
Algum de seus negócios extra-legais está sendo monitorado pela polícia. O que você tem que fazer para tirá-los do seu rastro?

## Reação da audiência:
Marque experiência quando a _abuela_ reage com horror às suas ações.

## Movimentos (escolha dois)

### [&emsp;] _Plata o plomo_
Quando você **Exige o que você merece** e você está em superioridade física, seu alvo marca dois estresses adicionais se ele não conceder à sua vontade.

### [&emsp;] Rancoroso
Quando você marcar o quarto estresse, você marca tanto _Amargurado_ quanto _Irado_. Quando você marcar o sexto estresse, receba +1 continuamente para causar dano àqueles que o merecerem.

### [&emsp;] Paixão Perigosa
Quando você colericamente expressar seu amor, faça um rolamento de **Expressar seu amor passionalmente**, mas adicione todas as questões de **Agredir alguém com voz ou violência** à lista. Você só pode obter no máximo +2 das quatro questões combinadas.

### [&emsp;] Capangas
Você tem uma pequena equipe pronta para executar seus planos, algo entre 4 e 6 pessoas. Dê a eles nomes. Quando você os enviar para conseguir dinheiro ou causar dor, role com as questões:

+ É um trabalho fácil?  
+ A equipe está com boa saúde?  

Em um acerto, eles aparecem para pressionar o seu alvo. Em um 7-9, seu alvo escolhe 1. Em um 10+, seu alvo escolhe 2:

+ Seu alvo dá à sua equipe o que eles vieram buscar;  
+ Sua equipe espanca o alvo; o alvo marca dois estresses;  
+ Sua equipe lembra ao alvo a quem este deve ser leal; seu alvo concede a você uma Influência.  

Em uma falha, seu alvo evita sua equipe e encontra provas de que eles trabalham para você. Diga-nos que provas seriam estas.

## Estresses [&emsp;] [ ★ ] [&emsp;] [ ◆ ] [ ★ ] [ ◆ ] [&emsp;] [ M ]

Para cada ★ marque um:

[&emsp;] Distraído (-2 para **Lidar com Sentimentos** & **Perceber Algo**  
[&emsp;] Desgastado (-2 para **Expressar Amor** & **Exigir o que Você Merece**  

Para cada ◆ marque um:

[&emsp;] Amargurado (+1 para **Acusar Alguém de Estar Mentindo**)  
[&emsp;] Irado (+1 para **Agredir com Voz ou Violência**)  

Quando você atingir **M**, desmarque todo o seu estresse e escolha um:

+ Gaste toda a sua Influência sobre os outros;  
+ Conceda uma Influência para todos os outros;  
+ Seja retirado de jogo.  

## Influência
______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

## Avanço
[&emsp;] [&emsp;] [&emsp;] [&emsp;] [ ▶ ] *Avanço*

[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha mais três adereços  
