# O Gêmeo (_El Gemelo_)

![O Gêmeo](images/image-004-150.png)

Antes de começar a fazer seu personagem, pergunte aos outros jogadores se eles estão dispostos a ser o seu gêmeo. Escolha entre os personagens dos jogadores que concordarem, ou elabore conjuntamente com o MC um personagem do narrador apropriadamente importante!

## Escolha um nome parecido com o nome de seu gêmeo:

## Sua Questão:
Você está se aproveitando da reputação de seu gêmeo?

## Aparência
Quais são as duas coisas que as pessoas notam em você?

## Escolha até três adereços:
| Adereços | Adereços |
| :--- | :--- |
| [&emsp;] A casa do seu gêmeo | [&emsp;] Um bigode ou cavanhaque |
| [&emsp;] Um carro esporte preto | [&emsp;] Uma foto de seu irmão e você |
| [&emsp;] As chaves de seu gêmeo | [&emsp;] Um lenço ou cachecol arrojado |
| [&emsp;] Um porão secreto | [&emsp;] Dois telefones celulares |
| [&emsp;] Um sorriso zombeteiro e maligno para a câmera | [&emsp;] Um conjunto de maquiagens guardado |

## Relacionamentos:
______ conhece você por quem você é. Diga como essa pessoa consegue distinguir você de seu irmão.  
______ teve um momento intenso com você. Que momento foi esse, e essa pessoa sabia que era você?   

## Neste capítulo:
Há um grande evento previsto para acontecer no qual se espera que seu gêmeo compareça. Como você se certificou que você também estará presente e por qual motivo você irá?

## Reação da audiência:
Marque experiência quando o pai ficar confuso sobre quem você é ou o que você está planejando.

## Movimentos (escolha dois)

### [&emsp;] Quase o mesmo
Quando você **expressar seu amor passionalmente** para alguém que seu gêmeo ama, trate um 6- como 7-9, ou um 7-9 como um 10+.

### [&emsp;] Tecelão de Histórias
Quando você timidamente tentar descobrir algo sobre alguém enquanto finge ser seu gêmeo, role com as questões:

+ Esse alguém ama meu gêmeo?  
+ Há alguma grande distração?  

Em um 7-9, reserve 1. Em um 10+, reserve 3. Gaste sua reserva em uma base um para uma fim de perguntar as seguintes questões:

+ Quando meu gêmeo e você dormiram juntos?  
+ O que você sente por meu gêmeo?  
+ O que você deve ao meu gêmeo?  
+ Como eu posso fazer você ficar com raiva de meu gêmeo?  

Em uma falha, reserve 1, mas seu alvo e seu gêmeo ganham uma Influência sobre você.

### [&emsp;] Não era eu
Quando alguém tiver um flashback envolvendo você ou seu gêmeo, gaste uma Influência sobre quem iniciou o flashback para substituir um pelo outro.

### [&emsp;] Laços Gêmeos
Quando você estiver fingindo ser seu gêmeo, você pode gastar uma Influência que ele possua sobre alguém como se fosse sua. Se você fizer isso, seu gêmeo recebe duas Influências sobre você.

## Estresses [&emsp;] [ ★ ] [&emsp;] [ ◆ ] [ ★ ] [ ◆ ] [&emsp;] [ M ]

Para cada ★ marque um:

[&emsp;] Distraído (-2 para **Lidar com Sentimentos** & **Perceber Algo**  
[&emsp;] Desgastado (-2 para **Expressar Amor** & **Exigir o que Você Merece**  

Para cada ◆ marque um:

[&emsp;] Amargurado (+1 para **Acusar Alguém de Estar Mentindo**)  
[&emsp;] Irado (+1 para **Agredir com Voz ou Violência**)  

Quando você atingir **M**, desmarque todo o seu estresse e escolha um:

+ Gaste toda a sua Influência sobre os outros;  
+ Conceda uma Influência para todos os outros;  
+ Seja retirado de jogo.  

## Influência
______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

## Avanço
[&emsp;] [&emsp;] [&emsp;] [&emsp;] [ ▶ ] *Avanço*

[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha mais três adereços 
