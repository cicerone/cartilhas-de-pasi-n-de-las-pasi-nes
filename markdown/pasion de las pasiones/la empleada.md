# A Empregada (_La Empleada_)

![A Empregada](images/image-003-150.png)

## Escolha um nome:
Aurora, Betty, Blanca, Jane, Maria, Reina, Rocio, Xiomara

## Sua questão:
Isso está subindo à cabeça?

## Aparência:
Quais são as duas coisas que as pessoas notam em você (apesar de sua beleza subestimada)?

## Escolha até três adereços:
| Adereços | Adereços |
| :--- | :--- |
| [&emsp;] Seu pequeno quarto de empregada | [&emsp;] Uma carta incriminadora |
| [&emsp;] Um vestido surpreendentemente justo | [&emsp;] Um suporte giratório para flores |
| [&emsp;] Um carrinho cheio de flores | [&emsp;] Um objeto criativo que você ama |
| [&emsp;] Um chaveiro grande e barulhento | [&emsp;] Um diário florido |
| [&emsp;] Uma conta bancária zerada | [&emsp;] Uma arma de fogo descarregada |

## Relacionamentos:
Você teve uma pincelada de romance com ______. Estamos falando de olho no olho, suavemente se inclinando um em direção ao outro, lábios quase... Por que não aconteceu?  
Isso é mais profundo do que você imaginaria... Você pegou ______ e ______ em um momento de maquinações. O que eles estavam planejando?  

## Neste capítulo:
Você escutou por alto, na lojinha de roupas do _barrio_, que alguém que você ama corre perigo mortal. Quem é a ameaça e como você poderá detê-la?

## Reação da audiência:
Marque experiência quando a _abuela_ disser, "_Pobrecita..._"

## ASPECTO: seus Pretendentes
Escolha duas pessoas (idealmente personagens de outros jogadores) para serem seus Pretendentes: eles estão ativamente lhe cortejando, mesmo se você estiver em um relacionamento. No início do jogo, escolha um para ser seu Amor.

______ [&emsp;] ♥ ______ [&emsp;] ♥

Receba +1 continuamente com seu Amor quando você estiver agindo para fortalecer o relacionamento.

Quando você compartilha intimidade ou sentimentos com o outro Pretendente, marque-o como seu Amor e remova a marcação de seu Amor anterior. Se você iniciou a intimidade, apague um estresse. Se ele iniciou a intimidade, ele recebe uma Influência sobre você.

A qualquer momento que você termine com um Pretendente, você pode romper seus laços com ele (apague o nome dele) e escolher uma outra pessoa para ocupar seu lugar.

## Movimentos (escolha um)

### [&emsp;] Chaves do Castelo
Quando você estiver cuidando de seus assuntos e por acaso escutar alguma coisa, role com as questões:

+ Você trabalha aqui?  
+ Você está vestida conforme o esperado para o lugar?  

Em um acerto, você consegue ir embora, mas alguém lhe viu; escolha quem e conceda Influência a ele. Em um 10+, você consegue ir embora sem ser visto, ou você pode fazer uma pergunta aos conspiradores, sua escolha. Em uma falha, você tropeça, derruba algo, ou de alguma outra forma revela sua presença. Você está no meio da cena agora.

### [&emsp;] _Confesión_
Quando você conta para alguém suas transgressões, role com as questões:

+ Você vê esse alguém como tendo coração puro?  
+ Você recorre religião ou família?  

Em um acerto, apague um estresse e conceda para esse alguém uma Influência. Em um 7-9, escolha um. Em um 10+, escolha dois:

+ Você desabafa; apague um estresse adicional;  
+ Você pede perdão para esse alguém; ele apaga um estresse;  
+ Você não fala tudo; não conceda a ele uma Influência.  

Em uma falha, a imagem que esse alguém tem de você fica manchada; ele ganha uma Influência sobre você, e você deve marcar dois estresses ou perder sua Influência sobre ele.

### [&emsp;] O Lado Barra Pesada da Cidade
Quando você foge de alguém em direção ao _barrio_, role com as questões:

+ Você está socialmente abaixo desse alguém?  
+ Não há ninguém diretamente lhe impedindo de fugir?  

Em um 7-9, você consegue fugir e escolhe um; em um 10+, você foge e escolhe dois:

+ Você consegue um novo adereço de sua lista ou alguma coisa de sua casa;  
+ Você se sente confortável nas luzes mais fracas; apague dois estresses;  
+ Ninguém viu para onde você foi; seu mistério concede a você uma Influência sobre alguém na cena.

## Estresses [&emsp;] [ ★ ] [&emsp;] [ ◆ ] [ ★ ] [ ◆ ] [&emsp;] [ M ]

Para cada ★ marque um:

[&emsp;] Distraído (-2 para **Lidar com Sentimentos** & **Perceber Algo**  
[&emsp;] Desgastado (-2 para **Expressar Amor** & **Exigir o que Você Merece**  

Para cada ◆ marque um:

[&emsp;] Amargurado (+1 para **Acusar Alguém de Estar Mentindo**)  
[&emsp;] Irado (+1 para **Agredir com Voz ou Violência**)  

Quando você atingir **M**, desmarque todo o seu estresse e escolha um:

+ Gaste toda a sua Influência sobre os outros;  
+ Conceda uma Influência para todos os outros;  
+ Seja retirado de jogo.  

## Influência
______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

## Avanço
[&emsp;] [&emsp;] [&emsp;] [&emsp;] [ ▶ ] *Avanço*

[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha mais três adereços  
