# A Matriarca (_La Doña_)

![A Matriarca](images/image-002-150.png)

## Escolha um nome:
Altagracia, Anjélica, Bernarda, Camila, Carimina, Graciela, Valencia

## Sua questão:
Isto é em interesse de alguém?

## Aparência:
Quais são as duas coisas que as pessoas notam em você?

## Escolha até três adereços:
| Adereços | Adereços |
| :--- | :--- |
| [&emsp;] Uma bela mansão | [&emsp;] Um carro com motorista |
| [&emsp;] Uma cigarreira | [&emsp;] Um gato ou cachorro de estimação |
| [&emsp;] Uma agenda telefônica cheia | [&emsp;] Uma taça de vinho fino |
| [&emsp;] Uma bolsa surpreendentemente cheia | [&emsp;] Um corte de cabelo de elite |
| [&emsp;] Toneladas de joias | [&emsp;] Afiliação a um clube de campo |

## Relacionamentos:
Você deu alguns bons conselhos para ______. Conte-nos como o conselho funcionou e como você se beneficiou disso.  
Você e ______ disputam entre si já faz muito tempo. Qual é seu problema com essa pessoa?  

## Neste capítulo:
Você está em posição de fazer um grande negócio. Quem você tem cultivado para lhe ajudar?

## Reação da audiência:
Marque experiência quando a filha adolescente exasperar-se com suas intromissões.

## Movimentos (escolha dois)

### [&emsp;] Isso já foi bonito
Quando você **Experimentar um flashback para um momento em que misturou negócios com prazer**, gaste de 1 a 3 Influências e role somando a quantidade de Influência gasta. Em um 7-9, seu alvo escolha um. Em um 10+, seu alvo escolhe 2:

+ Ele se entrega a você e você tem provas;  
+ Você extrai evidências irrefutáveis de um crime;  
+ Ele ainda está obcecado por você; ele descarta toda a Influência que tinha sobre você, e você obtém 1 Influência sobre ele.  

Em uma falha, escolha as opções como se este movimento tivesse sido usado contra você com um resultado 10+.

### [&emsp;] Que criança mimada
Quando você usar a vergonha de uma pessoa para manipulá-la a fim de fazer algo que lhe beneficie, adicione +1 na sua rolagem.

### [&emsp;] Conte-me tudo
Quando você se oferecer para assumir o fardo da culpa de alguém, pergunte se esse alguém irá aceitar ou rejeitar sua ajuda. Caso esse alguém aceite, ele desmarcará um estresse e você obterá 2 Influências sobre esse alguém. Se ele não aceitar, esse alguém deve descartar 1 Influência.

### [&emsp;] Diga a Epifanio...
Quando você pedir a uma pessoa para se encontrar com você pelo telefone ou por meio de um intermediário, role com as questões:

+ Você conhece o passado dessa pessoa?  
+ Vocês já foram íntimos?  

Em um sucesso, a pessoa concorda em se encontrar com você para discutir o assunto de boa fé. Em um 7-9, escolha 1. Em 10+, escolha 2:

+ A pessoa vem sozinha;  
+ Você pode levar reforços;  
+ A pessoa traz o que você disser para ela trazer;  
+ Você não precisa levar nada para a pessoa.  

Em uma falha, a pessoa concorda em se encontrar com você, mas será ela quem terá controle sobre quando, onde e como o encontro se dará.

## Estresses [&emsp;] [ ★ ] [&emsp;] [ ◆ ] [ ★ ] [ ◆ ] [&emsp;] [ M ]

Para cada ★ marque um:

[&emsp;] Distraído (-2 para **Lidar com Sentimentos** & **Perceber Algo**  
[&emsp;] Desgastado (-2 para **Expressar Amor** & **Exigir o que Você Merece**  

Para cada ◆ marque um:

[&emsp;] Amargurado (+1 para **Acusar Alguém de Estar Mentindo**)  
[&emsp;] Irado (+1 para **Agredir com Voz ou Violência**)  

Quando você atingir **M**, desmarque todo o seu estresse e escolha um:

+ Gaste toda a sua Influência sobre os outros;  
+ Conceda uma Influência para todos os outros;  
+ Seja retirado de jogo.  

## Influência
______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

## Avanço
[&emsp;] [&emsp;] [&emsp;] [&emsp;] [ ▶ ] *Avanço*

[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha mais três adereços  
