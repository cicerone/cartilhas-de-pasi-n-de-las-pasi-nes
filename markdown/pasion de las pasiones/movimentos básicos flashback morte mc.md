# Movimentos Básicos

## Exigir o que você merece
Quando você **Exige o que você merece**, role com as questões:

+ Você está oferecendo algo de valor em troca?  
+ A pessoa de quem você exige o ama neste momento?  

Em um acerto, o alvo de sua exigência escolhe um (se for um personagem do narrador, o MC escolherá por ele). Em um 10+, remova uma das opções da lista e então seu alvo escolhe uma das restantes:

+ Seu alvo cede à sua vontade; ele pode apagar um estresse ou você concede uma Influência para ele, conforme ele escolher;  
+ Seu alvo escalona a situação e rejeita sua exigência; ele marca um estresse;  
+ Seu alvo negocia sua exigência; ele concede a você uma Influência;  
+ Seu alvo foge; você pode pegar algo dele.  

## Expressar seu amor passionalmente
Quando você **Expressa seu amor passionalmente**, role com as questões:

+ Você está vestido para impressionar?  
+ A pessoa para quem você está se expressando acredita que você está solteiro?  

Em um 7-9, seu alvo escolhe um. Em um 10+, ele escolhe dois:

+ Seu alvo se entrega emocionalmente a você;  
+ Seu alvo revela um segredo que não deveria revelar;  
+ Seu alvo marca um estresse para esconder seus sentimentos;  
+ Seu alvo concede a você uma Influência e lhe mostra como você o cativou.  

## Agredir alguém
Quando você **Agride alguém com voz ou violência**, role com as questões:

+ Você pegou seu alvo desprevenido?  
+ Seu alvo causou-lhe algum mal?  

Em um acerto, você inflige dois estresses em seu alvo mas concede a ele uma Influência por mostrar seus sentimentos. Em um 7-9, escolha um. Em 10+, escolha dois:

+ Você fere seu alvo profundamente; inflija um estresse adicional;  
+ Você se apropria de algo que pertence ao seu alvo;  
+ Você sai com sua dignidade intacta; cancele a concessão de Influência para seu alvo;  
+ Você regojiza com a dor causada; apague um estresse.  

## Acusar alguém de mentir
Quando você **Acusa frontal e diretamente alguém de estar mentindo**, role com as questões:

+ Existe público para sua acusação?  
+ Você possui evidências?  

Em um 7-9, escolha dois. Em um 10+, escolha três:

+ Você está certo;  
+ Seu alvo admite estar mentindo ou marca um estresse (à escolha dele);  
+ Seu alvo foi surpreendido, assustado ou confuso; ele deve **Agir em desespero** antes de poder agir contra você;  
+ Reduza o controle de seu alvo sobre você; remova uma Influência sobre você.  

## Agir em desespero
Quando você **Agir em desespero**, diga ao MC de qual situação você pretende se evadir, e então role com as questões:

+ Você está fazendo isso por amor?  
+ Você está fazendo isso por vingança?  

Em um 10+, tudo corre bem. Em um 7-9, o MC lhe dará uma consequência desagradável ou uma escolha difícil.

## Notar algo estranho
Quando você tentar **Notar algo estranho** em uma interação social ou no espaço pessoal de alguém, role com as questões:

+ Você teve alguma intimidade com essa pessoa recentemente?  
+ Você está livre de outras distrações?  

Em um 7-9, faça uma pergunta. Em um 10+, faça duas. Em todo caso, receba +1 adiantado quando for agir com base nas informações obtidas.

+ Como seu personagem se sente sobre ______?  
+ O que você está planejando?  
+ Como eu consigo que seu personagem ______?  
+ O que você tem de valioso ou útil?  
+ O que seu personagem espera conseguir de ______?  

## Analisar suas emoções em voz alta
Quando você **Analisar suas emoções em voz alta**, pergunte a cada membro da audiência se eles estão torcendo por você, cada um concedendo a você um +1 ou um +0 (máximo de +3), e role. Em um acerto, escolha um. Receba +1 contínuo enquanto estiver perseguindo essa informação.

+ A audiência revela um evento crucial acontecendo fora da cena;  
+ A audiência conta para você como você talvez encontre o amor com um parceiro da escolha dela;  
+ A audiência conta para você como você pode conseguir ______ para ______.  

Em um 10+, arme a cena em que você persegue essa informação imediatamente.

# Movimentos de Flashback

## Flashback de um acordo
Quando você **Experimentar um flashback de um acordo feito com alguém**, gaste 1-3 Influências que você tenha sobre alguém e role usando a Influência gasta como bônus. Em um acerto, experimente um flashback do momento em que o acordo é feito. Em um 10+, escolha dois. Em um 7-9, escolha um, e seu parceiro no acordo escolhe o outro:

+ Diga o que seu parceiro prometeu;  
+ Diga o que você prometeu;  

Em uma falha, seu parceiro descreve ambos, ou ele descreve como ele usou o acordo para elaborar uma armadilha contra você que vai acontecer agora.

## Flashback de preparação
Quando você **Experimentar um flashback de uma preparação feita anteriormente**, gaste 1-3 estresses e role usando o estresse gasto como bônus. Escolha um, e diga ao MC do que se trata:

+ Você produziu uma evidência;  
+ Você arranjou para que uma pessoa esteja no lugar certo e na hora certa;  
+ Você escondeu algo.  

Em um acerto, você conseguiu usar sua preparação. Em um 10+, receba +1 adiantado quando usar sua preparação. Em uma falha, você pensou que a preparação estava feita, mas o MC revela que havia alguém espreitando à distância. O espreitador contará o que ele fez para interferir assim que o flashback terminar.

## Revelar uma verdade chocante
Quando você **Experimentar um flashback para revelar uma verdade chocante sobre um outro personagem de jogador**, gaste três Influências que você possua sobre esse personagem e role usando o bônus de limiar de estresses marcados. Em um acerto, a informação é desconcertante; antes de poder agir contra você, seu alvo deve **Agir em desespero** com -2 adiantados. Em um 7-9, escolha um. Em um 10+, escolha dois:

+ Você tem evidência irrefutável da verdade revelada;  
+ A verdade chocante torna válida sua reivindicação sobre algo que seu alvo valoriza;  
+ Você introduz um novo personagem surpreendente que lhe apoia.  

Em uma falha, o tiro sai pela culatra – marque três estresses.

# Encarar a Morte Certa
Este não é um jogo de matar, pilhar e destruir, portanto não espere ficar rodeado pelos corpos de seus inimigos. Entretanto, quando as paixões ardem, as coisas podem sair horrivelmente errado. Quer seja um tiro em um jardim escuro ou uma xícara de café com cianureto, os personagens de telenovela enfrentam mortes certas e horríveis todo o tempo. Isto não significa que você rolará os dados para encarar a morte certa a cada vez que as coisas fiquem perigosas, apenas quando não houver meios para a pessoa escapar dela.

Quando você **Encara a morte certa**, role adicionando o limiar de estresse como bônus. Em uma falha, sua morte é menos certa do que imaginávamos; diga-nos como foi sua fuga ousada dela. Em um 7-9, escolha um da lista. Em um 10+, a audiência escolhe um da lista. Quando uma opção é escolhida, ela é riscada e ninguém mais pode escolhê-la novamente.  

+ Alguém o salva a tempo; você concede a ele 3 Influências;  
+ Um de seus adereços recebe o ataque; perca-o imediatamente;  
+ Adquira uma cicatriz marcante, porém sensual;  
+ Ganhe uma debilidade permanente (perna manca, tapa-olho, etc.);  
+ Torne-se uma versão maligna e distorcida de você mesmo;  
+ Sofra de amnésia;  
+ Retorne com uma nova face;  
+ Acabe em um (possivelmente) coma longo;  
+ Acabe realmente morto; esta opção pode ser escolhida múltiplas vezes.  

# Para o Mestre de Cerimônias

## Agendas
+ Faça o drama da telenovela parecer importante;   
+ Complique os relacionamentos dos personagens dos jogadores;  
+ Jogue para descobrir o que acontece;  

## Princípios
+ Faça os cenários deslumbrantes e bonitos;  
+ Dirija-se aos personagens, não aos jogadores;  
+ Faça seus movimentos, mas indiretamente;  
+ Faça seus movimentos, mas jamais diga os nomes deles;  
+ Dê um preço a tudo e recompense o sacrifício;  
+ Preencha a vida dos personagens com segredos;  
+ Rodeie os jogadores com traições e dúvidas;  
+ Seja fã dos personagens dos jogadores;  
+ Nunca deixe seus jogadores entrarem em suas zonas de conforto;  
+ Às vezes, determine que seus jogadores tomem decisões.  

## Movimentos do MC
+ Infligir estresse (conforme estabelecido);  
+ Por as pessoas juntas ou separá-las;  
+ Cruzar os limites entre as esferas pública e privada;  
+ Deixar a pessoa errada saber o que está acontecendo;  
+ Por alguém em cena;  
+ Perturbar a rotina de alguém;  
+ Fazer a alguém uma oferta para conseguir o que quer;  
+ Oferecer imunidade em troca de desistir do amor;  
+ Virar as costas para alguém;  
+ Debruçar-se sobre um segredo;  
+ Demandar que alguém faça a sua parte;  
+ Após cada movimento, perguntar "o que você faz?"  

## Lista de Prenomes
Alejandro, Angel, Antonella, Bernardo, Camila, Carlos, Catalina, Elena, Fabian, Felipe, Florencia, Gabriel, Giovanna, Hector, Isabella, Jaime, Joaquín, Lucía, Luis, Manuela, Martina, Mateo, Maximiliano, Milagros, Nicolás, Paula, Santiago, Sofía, Thiago, Valentina, Vicente, Ximena.

## Lista de Sobrenomes
Aguilar, Blanco, Castillo, Díaz, Estrada, Flores, García, González, Jiménez, León, López, Luna, Morales, Núñez, Ortiz, Pérez, Reyes, Rivera, Rodríguez, Santos, Vásquez.

## Exemplos de Lugares
O bar de um hotel luxuoso, a piscina em O Clube, a sala de estar de uma mansão, a varanda virada para o mar, o cais ao por do sol, no _barrió_, exatamente depois da segurança do aeroporto, a proa de um iate, a sala de espera ou de recuperação de um hospital.

## Influências dos NPCS
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
______ deve a ______ __ Influência
