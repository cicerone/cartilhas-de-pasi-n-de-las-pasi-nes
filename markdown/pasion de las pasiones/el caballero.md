# O Cavalheiro (_El Caballero_)

![O Cavalheiro](images/image-001-150.png)

## Escolha um nome:
Álvaro, Domingo, José, Lorenzo, Mariano, Miguel

## Sua questão:
Você está lidando com as coisas diretamente e sem astúcia?

## Aparência:
Quais são as duas coisas que as pessoas notam em você?

## Escolha até três adereços:
| Adereços | Adereços |
| :--- | :--- |
| [&emsp;] O bar local | [&emsp;] O veleiro de seu _abuelo_ |
| [&emsp;] Um trabalho na polícia | [&emsp;] Um cavalo ou motocicleta |
| [&emsp;] Um contato que você não queria ter | [&emsp;] Uma fotografia antiga |
| [&emsp;] Um lugar seguro para uma pessoa | [&emsp;] Um terno que você não usa |
| [&emsp;] Uma arma de fogo que você odeia usar | [&emsp;] Algum tipo de cicatriz sensual |

## Relacionamentos:
Você se envolveu em uma briga feia com ______. Conte-nos como foi.  
______ conhece você de uma época em que você era diferente. Você era mais civilizado ou mais selvagem?  

## Neste capítulo:
Você está aqui para corrigir algo errado. Alguém violou leis, sejam as da sociedade ou as suas. Quem é esse violador, o que ele fez, e o que você vai fazer sobre isso?

## Reação da audiência:
Marque experiência quando a garota adolescente da audiência se apaixonar um pouco por você.

## Movimentos (escolha dois)

### [&emsp;] Posicionar-se
Quando você interferir em uma cena para proteger alguém, role com as questões:

+ Você está tentando impressionar alguém?  
+ A lei está sendo violada?  

Em um 7-9, reserve 1. Em um 10+, reserve 3. Enquanto você estiver se mantendo na defesa desse alguém, gaste a reserva na base de 1 para 1 a fim de:

+ Proteger esse alguém de um ataque; receba o estresse no lugar dele;  
+ Impedir alguém de sair de um cômodo a não ser que passe por cima de você;  
+ Receba +1 para **Agredir com Violência**  

Em uma falha, você mostrou suas intenções cedo demais; você está à mercê do agressor, e ele pode infligir estresse em você ou no alvo original.

### [&emsp;] Grandiloquência
Quando você **Expressar seu amor passionalmente** com um grande gesto de amor, o alvo do movimento deve escolher uma opção a mais da lista, mesmo em caso de falha.

### [&emsp;] Para a sua proteção
Quando você for acusado de mentir, marque um estresse para reduzir em 2 a rolagem do acusador caso a acusação possa por em risco alguém sob sua proteção.

### [&emsp;] Escalonar
Quando você tentar **Notar Algo Estranho**, pergunte a seguinte questão mesmo em caso de falha:

+ Onde está a arma mais próxima?  

## Estresses [&emsp;] [ ★ ] [&emsp;] [ ◆ ] [ ★ ] [ ◆ ] [&emsp;] [ M ]

Para cada ★ marque um:

[&emsp;] Distraído (-2 para **Lidar com Sentimentos** & **Perceber Algo**  
[&emsp;] Desgastado (-2 para **Expressar Amor** & **Exigir o que Você Merece**  

Para cada ◆ marque um:

[&emsp;] Amargurado (+1 para **Acusar Alguém de Estar Mentindo**)  
[&emsp;] Irado (+1 para **Agredir com Voz ou Violência**)  

Quando você atingir **M**, desmarque todo o seu estresse e escolha um:

+ Gaste toda a sua Influência sobre os outros;  
+ Conceda uma Influência para todos os outros;  
+ Seja retirado de jogo.  

## Influência
______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

______ deve a você __ Influência

## Avanço
[&emsp;] [&emsp;] [&emsp;] [&emsp;] [ ▶ ] *Avanço*

[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de sua cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha um movimento novo de uma outra cartilha  
[&emsp;] Escolha mais três adereços 
